#!/bin/sh

xrdb merge ~/.Xresources
dunst &
flameshot &
xsettingsd &
xbacklight -set 10 &
/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &
sudo wg-quick up shop &
# feh --bg-fill --no-fehbg ~/Pictures/Wallpapers/alonetogether2.png &
xwallpaper --tile "$HOME/Pictures/Wallpapers/tiledgray.jpg" &
xset r rate 180 50 &
picom --legacy-backends &
~/.config/chadwm/scripts/bar.sh &
setxkbmap -option "caps:escape" &

if [ -d /etc/X11/xinit/xinitrc.d ]; then
	for f in /etc/X11/xinit/xinitrc.d/?*.sh; do
		[ -x "$f" ] && . "$f"
	done
	unset f
fi

while type dwm >/dev/null; do dwm && continue || break; done
